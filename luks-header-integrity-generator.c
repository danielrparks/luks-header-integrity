/* MIT License

Copyright (c) 2021 Daniel Parks

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

#define WHITESPACE " \t\f\n"
#define OPTNAME "header-hash="

char *progname;

char *get_hash(char *optstring) {
	char *tok;
	while ((tok = strtok(optstring, ","))) {
		if (!strncmp(tok, OPTNAME, strlen(OPTNAME))) {
			return tok + strlen(OPTNAME);
		}
		optstring = NULL;
	}
	return NULL;
}

char *get_path(char *path) {
	if (!strncmp(path, "UUID=", 5)) {
		int len = strlen(path) - 5;
		int plen = strlen("/dev/disk/by-uuid/");
		char *newpath = malloc(len + plen + 1);
		strcpy(newpath, "/dev/disk/by-uuid/");
		strcpy(newpath + plen, path + 5);
		return newpath;
	} else {
		return path;
	}
}

char *escape(char *str) {
	int len = strlen(str);
	while (str[0] == '/') { str++; len--; }
	while (str[len-1] == '/') { str[len-1] = 0; len--; }
	char *next = malloc(len * 5 / 2);
	char *nextp = next;
	char c = str[0];
	for (int i = 0; i < len; i++, c = str[i]) {
		if (c == '/') {
			*nextp = '-';
			nextp++;
		} else if (
				   (c >= 'a' && c <= 'z')
				|| (c >= 'A' && c <= 'Z')
				|| (c >= '0' && c <= '9')
				||  c == ':' || c == '_' || c == '.') {
			*nextp = c;
			nextp++;
		} else {
			snprintf(nextp, 5, "\\x%02x", c);
			nextp += 4;
		}
	}
	*nextp = 0;
	return next;
}

int generate_unit(char *runtime_dir, char *name, char *path, char *hash) {
	name = escape(name);
	char *unit = malloc(strlen(runtime_dir) + strlen("/luks-header-integrity@") + strlen(name) + strlen(".service") + 1);
	char *unit_basename = unit + strlen(runtime_dir) + 1;
	sprintf(unit, "%s/luks-header-integrity@%s.service", runtime_dir, name);
	char *reverse_dependency = malloc(strlen(runtime_dir) + strlen("/systemd-cryptsetup@.service.requires/") + strlen(name) + strlen(unit_basename) + 1);
	sprintf(reverse_dependency, "%s/systemd-cryptsetup@%s.service.requires%c%s", runtime_dir, name, 0, unit_basename);
	char *escaped_devunit, *devunit = malloc(strlen(path) + strlen(".device") + 1);
	sprintf(devunit, "%s.device", path);
	escaped_devunit = escape(devunit);
	int r, serrno;
	FILE *funit = fopen(unit, "w");
	if (!funit) return -1;
	r = fprintf(funit,
		"[Unit]\n"
		"Description=Verify LUKS Header for %%I\n"
		"DefaultDependencies=no\n"
		"IgnoreOnIsolate=yes\n"
		"After=cryptsetup-pre.target systemd-udevd-kernel.socket\n"
		"Before=systemd-cryptsetup@%%i.service\n"
		"BindsTo=%s\n"
		"After=%s\n"
		"\n"
		"[Service]\n"
		"ExecStart=/usr/bin/luks-header-verify %s %s\n"
		"TimeoutStartSec=2\n"
		"Type=oneshot\n"
		"RemainAfterExit=yes\n"
		"\n"
		"[Install]\n"
		"RequiredBy=systemd-cryptsetup@%%i.service\n",
		escaped_devunit, escaped_devunit, path, hash);
	if (r < 0) return r;
	fclose(funit);
	r = mkdir(reverse_dependency, 0755);
	if (r != 0 && errno != EEXIST) return r;
	reverse_dependency[strlen(reverse_dependency)] = '/';
	r = symlink(unit, reverse_dependency);
	serrno = errno;
	free(name);
	free(unit);
	free(reverse_dependency);
	free(escaped_devunit);
	free(devunit);
	errno = serrno;
	return r;
}

int main(int argc, char *argv[]) {
	char *runtime_dir = "/tmp";
	if (argc == 4) {
		runtime_dir = argv[1];
	} else if (argc != 1) {
		fputs("This program requires 0 or 3 arguments.", stderr);
		exit(1);
	}
	progname = argv[0];
	char *crypttab = getenv("SYSTEMD_CRYPTTAB");
	if (!crypttab) crypttab = "/etc/crypttab";
	FILE *fcrypttab = fopen(crypttab, "r");
	if (!fcrypttab) {
		perror(progname);
		exit(0); // should not fail if there are no units to generate
	}
	ssize_t nread;
	char *line = NULL;
	size_t len = 0;
	char *fields[4];
	char *name, *path, *hash;
	while ((nread = getline(&line, &len, fcrypttab)) != -1) {
		if (nread == 0 || line[0] == '#' || line[0] == '\n') continue;
		for (int i = 0; i < 4; i++) {
			fields[i] = strtok(i ? NULL: line, WHITESPACE);
		}
		if (fields[3]) {
			hash = get_hash(fields[3]);
			if (!hash) continue;
			name = fields[0];
			path = get_path(fields[1]);
			if (generate_unit(runtime_dir, name, path, hash) < 0) {
				perror("generate_unit");
				exit(2);
			}
			free(path);
		}
	}
	fclose(fcrypttab);
	free(line);
	exit(0);
}
