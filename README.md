# luks-header-integrity

A systemd generator and mkinitcpio hook allowing the user to enforce a sha256 hash for disks defined in the crypttab

# Installing
[aur](https://aur.archlinux.org/packages/luks-header-integrity/)  
[make install](https://gitlab.com/danielrparks/luks-header-integrity/-/blob/main/Makefile#L13)

If there is not a package available for your distribution, I recommend creating one instead of using `make install`, which should really only be used in a pinch.

# How to use
Add the `sd-luks-verify` hook to your `mkinitcpio.conf`. Here's mine:
```
HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block sd-luks-verify sd-encrypt filesystems fsck)
```

Then, add the `header-hash=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx` option to your `crypttab`. Here's mine (censored):
```
cryptroot      UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx    -                       discard,x-initrd.attach,header-hash=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

These files are licensed under the [MIT License](LICENSE.md).
