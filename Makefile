CC=gcc
CFLAGS=-Wall -Werror -O2
PREFIX=/usr

all: luks-header-integrity-generator

luks-header-integrity-generator: luks-header-integrity-generator.c
	$(CC) $(CFLAGS) luks-header-integrity-generator.c -o luks-header-integrity-generator

clean:
	rm -f luks-header-integrity-generator

install:
	install -Dm0755 ./luks-header-verify $(PREFIX)/bin/luks-header-verify
	install -Dm0755 ./luks-header-integrity-generator $(PREFIX)/lib/systemd/system-generators/luks-header-integrity-generator
	install -Dm0644 ./sd-luks-verify $(PREFIX)/lib/initcpio/install/sd-luks-verify

uninstall:
	rm $(PREFIX)/bin/luks-header-verify
	rm $(PREFIX)/lib/systemd/system-generators/luks-header-integrity-generator
	rm $(PREFIX)/lib/initcpio/install/sd-luks-verify
